import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {ListItemModel, listItems} from "../../assets/data-model";

export enum KEY_CODE {
  UP_ARROW= 38,
  DOWN_ARROW = 40
}

@Component({
  selector: 'search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.scss', '../home/home.component.scss']
})
export class SearchComponentComponent implements OnInit {
  listItems: ListItemModel[] = listItems;
  finalisedListItems: any = listItems;
  searchTerm : any = null;
  selectedItem : any = null;

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    const index = this.finalisedListItems.indexOf(this.selectedItem);
    if(event['keyCode'] === KEY_CODE['UP_ARROW']){
      this.setSelectedItem(false, index);
    }
    if(event['keyCode'] === KEY_CODE['DOWN_ARROW']){
      this.setSelectedItem(true, index);
    }
  }
  constructor(private el : ElementRef) { }

  ngOnInit() {
    const invalidControl = this.el.nativeElement.querySelector(".ng-untouched");
    if (invalidControl) {
      invalidControl.focus();
    }
    if(!this.selectedItem){
      this.selectedItem = this.finalisedListItems[0];
    }
  }

  setSelectedItem(item, index): void {
    if(!item) {
      if(this.finalisedListItems[index - 1]) {
        this.selectedItem = this.finalisedListItems[index - 1];
      }
    } else {
      if(this.finalisedListItems[index + 1]) {
        this.selectedItem = this.finalisedListItems[index + 1];
      }
    }
    let el = this.el.nativeElement.querySelector('.active');
    if(el){
      el.scrollIntoView();
    }
  }

  searchChanged(event): void {
    if(event['keyCode'] != 38 && event['keyCode'] != 40) {
      if(this.searchTerm) {
        this.finalisedListItems = this.listItems.filter(item => {
          return item['name'].includes(this.searchTerm);
        });
      } else {
        this.finalisedListItems = this.listItems;
      }
      this.selectedItem = this.finalisedListItems[0];
    }
  }

}
