import { Component, OnInit, HostListener } from '@angular/core';
import {listItems, ListItemModel, tabs, TabsModel, ArtefactModel, artifacts} from "../../assets/data-model";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  showSearchContainer : boolean = false;
  myTabs : TabsModel[] = tabs;
  listItems: ListItemModel[] = listItems;
  artifacts: ArtefactModel[] = artifacts;

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if(event['keyCode'] === 84){
      this.showSearchContainer = true;
    }
    if(event['keyCode'] === 27) {
      this.showSearchContainer = false;
    }
  }

  constructor() { }

  ngOnInit() {
  }

  onClickSearch(): void {
    this.showSearchContainer = true;
  }

}
