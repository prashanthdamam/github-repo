import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {SearchComponentComponent} from "./search-component/search-component.component";


const routes: Routes = [
  {path : '', component: HomeComponent, pathMatch: 'full'},
  {path : 'home', component: HomeComponent},
  {path : 'search', component : SearchComponentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
