export interface TabsModel {
  label: string,
  disabled: boolean
}

export interface ArtefactModel {
  title: string,
  desc: string
}

export interface ListItemModel {
  name: string,
  desc: string,
  time: string,
  index : number
}

export const artifacts:ArtefactModel[] = [
  {
    title : 'About',
    desc : 'Provides HTML UI layout for Angular applications; using Flexbox and a Responsive API'
  },
  {
    title : 'Releases',
    desc : 'There are no releases as of now.'
  },
  {
    title : 'Contributors',
    desc : 'You are only the contributor.'
  }
]



export const tabs:TabsModel[] = [
  {
    label : 'Code',
    disabled : false
  },
  {
    label : 'Issues',
    disabled : true
  },
  {
    label : 'Pull Requests',
    disabled : true
  },
  {
    label : 'Actions',
    disabled : true
  },
  {
    label : 'Projects',
    disabled : true
  },
  {
    label : 'Wiki',
    disabled : true
  },
  {
    label : 'Security',
    disabled : true
  },
  {
    label : 'Insights',
    disabled : true
  }
];

export const listItems :ListItemModel[] = [
  {
    name: '.circleci',
    desc: 'build: fix gulp setup not working with node v12 (#1142)',
    time: '9 months ago',
    index: 1
  },
  {
    name: '.github',
    desc: 'build: fix gulp setup not working with node v12 (#1142)',
    time: '2 months ago',
    index: 2
  },
  {
    name: '.docs',
    desc: 'bdocs: fix HTML closing tags for fxFlex API page (#1225)',
    time: '3 months ago',
    index: 3
  },
  {
    name: 'guides',
    desc: 'feat(lib): add config options for flex basis and other tokens',
    time: '2 years ago',
    index: 4
  },
  {
    name: 'scripts',
    desc: 'build: switch to circleci (#905)',
    time: '2 years ago',
    index: 5
  },
  {
    name: 'src',
    desc: 'chore: update to 10.0.0-beta.32 with changelog (#1272)',
    time: 'last month',
    index: 6
  },
  {
    name: 'test',
    desc: 'build: switch to circleci (#905)',
    time: '2 years ago',
    index: 7
  },
  {
    name: 'tools',
    desc: 'build: upgrade to Angular v10, CDK v10, and TypeScript 3.9 (#1261)',
    time: '2 months ago',
    index: 8
  },
  {
    name: '.clang-format',
    desc: 'update(yarn, build, tests): prepare for testing',
    time: '4 years ago',
    index: 9
  },
  {
    name: '.editorconfig',
    desc: 'chore(config): update development configs (#31)',
    time: '4 years ago',
    index: 10
  },
  {
    name: '.gitattributes',
    desc: 'chore: force LF as linebreak-style when checking out the repo (#1145)',
    time: '9 months ago',
    index: 11
  },
  {
    name: '.gitignore',
    desc: 'update(build): upgrade to Angular v7.2 and TypeScript v3.2 (#976)',
    time: '2 years ago',
    index: 12
  },
  {
    name: 'CHANGELOG.md',
    desc: 'chore: update to 10.0.0-beta.32 with changelog (#1272)',
    time: 'last month',
    index: 13
  },
  {
    name: 'CODE_REVIEWS.md',
    desc: 'docs: cleanup the Wiki and add missing docs',
    time: '3 years ago',
    index: 14
  },
  {
    name: 'CODING_STANDARDS.md',
    desc: 'docs: change reference from RxJS "lettable" to "pipeable"',
    time: '3 years ago',
    index: 15
  },
  {
    name: 'CONTRIBUTING.md',
    desc: 'chore: update StackBlitz links (#1254)',
    time: '2 months ago',
    index: 16
  },
  {
    name: 'LICENSE',
    desc: 'chore: bump year (#1167)',
    time: '7 months ago',
    index: 17
  },
  {
    name: 'README.md',
    desc: 'chore: update StackBlitz links (#1254)',
    time: '2 months ago',
    index: 18
  },
  {
    name: 'build-config.js',
    desc: 'build: move angular version to package.json (#794)',
    time: '2 years ago',
    index: 19
  },
  {
    name: 'firebase.json',
    desc: 'chore(build): update build to match ngM2 build processes (#342)',
    time: '3 years ago',
    index: 20
  },
  {
    name: 'gulpfile.js',
    desc: 'chore(build): update build to match ngM2 build processes (#342)',
    time: '3 years ago',
    index: 21
  },
  {
    name: 'package.json',
    desc: 'chore: update to 10.0.0-beta.32 with changelog (#1272)',
    time: 'last month',
    index: 22
  },
  {
    name: 'stylelint-config.json',
    desc: 'feat(core): add static scss mixin (#940)',
    time: '2 years ago',
    index: 23
  },
  {
    name: 'tsconfig.json',
    desc: 'build: upgrade to Angular and Material v7 and add strict flags (#855)',
    time: '2 years ago',
    index: 24
  },
  {
    name: 'tslint.json',
    desc: 'feat(core): MediaObserver can report 1..n activations (#994)',
    time: '2 years ago',
    index: 25
  },
  {
    name: 'base',
    desc: 'fix(core): only trigger style updates when value changes (#1246)',
    time: '3 months ago',
    index: 26
  },
  {
    name: 'basis-validator',
    desc: 'feat(core): add validateBasis to core export (#706)',
    time: '2 years ago',
    index: 27
  },
  {
    name: 'breakpoints',
    desc: 'fix(orientation): use tablet landscape screen type (#1220)',
    time: '4 months ago',
    index: 28
  },
  {
    name: 'match-media',
    desc: 'fix(mock-match-media): ensure overlapping breakpoints are activated (#1271)',
    time: '2 months ago',
    index: 29
  },
  {
    name: 'media-marshaller',
    desc: 'fix(core): ignore null values in breakpoint fallback mechanism (#1247)',
    time: '3 months ago',
    index: 30
  },
  {
    name: 'media-observer',
    desc: 'fix(media-observer): return correct value for isActive on init (#1244)',
    time: '3 months ago',
    index: 31
  },
  {
    name: 'sass',
    desc: 'fix(core): update breakpoints ranges to avoid overlapping (#1075)',
    time: '14 months ago',
    index: 32
  },
  {
    name: 'style-builder',
    desc: 'feat(core): add memoization to style generation (#888)',
    time: '2 years ago',
    index: 33
  },
  {
    name: 'style-utils',
    desc: 'fix(flex): wait for parent element until template is initialized (#1237)',
    time: '3 months ago',
    index: 34
  },
  {
    name: 'stylesheet-map',
    desc: 'chore: remove deletion target candidates (#858)',
    time: '2 years ago',
    index: 35
  },
  {
    name: 'tokens',
    desc: 'feat(server): add ability to specify breakpoints for MediaObserver (#999',
    time: '2 years ago',
    index: 36
  },
  {
    name: 'utils',
    desc: 'fix(core): align breakpoints with those used in CDK (#1006)',
    time: '2 years ago',
    index: 37
  },
  {
    name: 'README.md',
    desc: 'feat(core): implement MediaTrigger to allow manual breakpoint activat…',
    time: '2 years ago',
    index: 38
  },
  {
    name: 'add-alias.ts',
    desc: 'feat(core): MediaObserver can report 1..n activations (#994)',
    time: '2 years ago',
    index: 39
  },
  {
    name: 'browser-provider.ts',
    desc: 'fix(lib): resolve RegExp Issue in older versions of Safari (#643)',
    time: '2 years ago',
    index: 40
  }
]





